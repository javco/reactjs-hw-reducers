import React, { useReducer } from 'react'

const ACTIONS = {
  DECREMENT: 'decrement',
  INCREMENT: 'increment'
}

function reducer(state, action) {
  switch (action.type) {
    case ACTIONS.DECREMENT:
      return { count: state.count - 1 }
    case ACTIONS.INCREMENT:
      return { count: state.count + 1 }
    default:
      return state
  }
}

const Counter = (props) => {

  const style = {
    fontSize: '32px',
    margin: '0 15px'
  };

  const [state, dispatch] = useReducer(reducer, { count : 0 })

  function decrement() {
    dispatch({ type: ACTIONS.DECREMENT })
  }
  function increment() {
    dispatch({ type: ACTIONS.INCREMENT })
  }

  return (
    <>
      <h2>simple counter with reducers</h2>

      <button onClick={decrement} style={style}>-</button>
      <span style={style}>{state.count}</span>
      <button style={style}  onClick={increment}>+</button>
    </>
  )
}

export default Counter