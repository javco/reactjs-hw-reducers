import React from 'react'

import { ACTIONS } from './ToDos.js'

const Todo = ({todo, dispatch}) => {
  return(
    <div>
      <h3 style={{ color: todo.complete ? 'green' : 'tomato' }}>{todo.name}</h3>
      <button
        onClick={ () => dispatch({
          type: ACTIONS.TOGGLE_TODO,
          payload: {
            id: todo.id
          }
        })}
        >toggle</button>
      <button
        onClick={ () => dispatch({
          type: ACTIONS.DEL_TODO,
          payload: {
            id: todo.id
          }
        })}
        >delete</button>
    </div>
  )
}

export default Todo