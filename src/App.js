import './App.css';

import Counter from './components/Counter';
import ToDos from './components/ToDos';

function App() {
  return (
    <div className="App">
      <Counter />
      <ToDos />
    </div>
  );
}

export default App;
